# - *- coding: utf- 8 - *-
import configparser

config = configparser.ConfigParser()
config.read("settings.ini")
BOT_TOKEN = config["settings"]["token"]
admins = config["settings"]["admin_id"]
SUBSCRIPTION_CHANNEL_ID = config["settings"]["subscription_channel_id"]
SUBSCRIPTION_CHANNEL_URL = config["settings"]["subscription_channel_url"]
BTN_REVIEWS_URL = config["settings"]["btn_reviews_url"]
BTN_CHAT_URL = config["settings"]["btn_chat_url"]
BTN_BOT_RESERV_URL = config["settings"]["btn_bot_reserv_url"]

if "," in admins:
    admins = admins.split(",")
else:
    if len(admins) >= 1:
        admins = [admins]
    else:
        admins = []
        print("***** Вы не указали админ ID *****")

bot_version = "1.1"
bot_description = f"<b>♻ Bot created by @cod21s</b>\n" \
                  f"<b>⚜ Bot Version:</b> <code>{bot_version}</code>\n" \
